using EmailNotification.Models;
using EmailNotification.Services;
using EmailNotification.Utilities;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add configuration
builder.Services.Configure<SmtpConnectionConfiguration>(builder.Configuration.GetSection("SmtpConnection"));

// Configure database connection
var serverVersion = new MySqlServerVersion(new Version(8, 0, 36));
var connectionString = builder.Configuration.GetConnectionString("EmailNotificationMySql");
builder.Services.AddDbContext<EmailNotificationDbContext>(options => options.UseMySql(connectionString, serverVersion));

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IMessageDispatchService, SmtpDispatchService>();

// Add controllers
builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapControllers();

app.Run();
