using System.Net.Mail;

namespace EmailNotification.Models.Entities;

public class Message
{
    public Guid Id { get; set; }
    public Guid TemplateId { get; set; }
    public Template Template { get; set; } = null!;
    public MailAddress Recipient { get; set; } = null!;
    public Dictionary<string, string> MessageData { get; set; } = null!;
    public DateTime? ScheduledSendDateTime { get; set; }
    public DateTime? ActualSendDateTime { get; set; }
}