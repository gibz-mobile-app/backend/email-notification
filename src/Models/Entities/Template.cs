namespace EmailNotification.Models.Entities;

public class Template
{
    public Guid Id { get; set; }
    public string NameIdentifier { get; set; } = null!;
    public string Subject { get; set; } = null!;
    public string Body { get; set; } = null!;
}