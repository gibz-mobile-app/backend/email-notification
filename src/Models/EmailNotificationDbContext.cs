
using System.Net.Mail;
using System.Text.Json;
using EmailNotification.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace EmailNotification.Models;

public class EmailNotificationDbContext(DbContextOptions<EmailNotificationDbContext> options) : DbContext(options)
{
    public DbSet<Template> Templates { get; set; }
    public DbSet<Message> EmailNotifications { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .Entity<Message>()
            .Property(message => message.Recipient)
            .HasConversion(
                v => v.ToString(),
                v => new MailAddress(v)
            );

        modelBuilder
            .Entity<Message>()
            .Property(message => message.MessageData)
            .HasConversion(
                v => JsonSerializer.Serialize(v, null as JsonSerializerOptions),
                v => JsonSerializer.Deserialize<Dictionary<string, string>>(v, null as JsonSerializerOptions)!
            );
    }

}