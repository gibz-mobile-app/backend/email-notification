using System.Net.Mail;
using System.Text.Json.Serialization;
using EmailNotification.Utilities;

namespace EmailNotification.Models;

public class MessageSchedulingRequest
{
    public string TemplateIdentifier { get; set; } = null!;
    
    [JsonConverter(typeof(MailAddressConverter))]
    public MailAddress Recipient { get; set; } = null!;
    
    public Dictionary<string, string> MessageData { get; set; } = null!;
    
    public DateTime? ScheduledSendDateTime { get; set; }
}