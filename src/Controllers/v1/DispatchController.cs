using EmailNotification.Models;
using EmailNotification.Models.Entities;
using EmailNotification.Services;
using HandlebarsDotNet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmailNotification.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class DispatchController(EmailNotificationDbContext dbContext, IMessageDispatchService _messageDispatchService) : ControllerBase
{
    private readonly EmailNotificationDbContext _dbContext = dbContext;

    private readonly Dictionary<Guid, CompiledHandlebarTemplate> _cachedTemplates = new Dictionary<Guid, CompiledHandlebarTemplate>();

    [HttpGet]
    public async Task<IActionResult> DispatchScheduledMessages()
    {
        // Retrieve pending notifications from database.
        var pendingNotifications = await _dbContext.EmailNotifications
            .Include(notification => notification.Template)
            .Where(notification => notification.ActualSendDateTime == null && (notification.ScheduledSendDateTime == null || notification.ScheduledSendDateTime <= DateTime.Now))
            .OrderBy(notification => notification.ScheduledSendDateTime)
            .Take(50)
            .ToListAsync();

        // Quit, if there are no pending messages to be dispatched.
        if (pendingNotifications.Count == 0)
        {
            return NoContent();
        }

        foreach (var pendingNotification in pendingNotifications)
        {
            // Compile template once for all mails to be sent
            var template = GetTemplate(pendingNotification.Template);

            // Deserialize notification specific data for rendering subject and body
            var data = pendingNotification.MessageData;
            var renderedSubject = template.Subject(data);
            var renderedBody = template.Body(data);

            try
            {
                await _messageDispatchService.SendEmailAsync(pendingNotification.Recipient, renderedSubject, renderedBody);
                pendingNotification.ActualSendDateTime = DateTime.Now;
            }
            catch (Exception)
            {
                // TODO: Log exception...
                throw;
            }
        }

        await _dbContext.SaveChangesAsync();

        return Ok();
    }

    private CompiledHandlebarTemplate GetTemplate(Template template)
    {
        if (_cachedTemplates.TryGetValue(template.Id, out CompiledHandlebarTemplate? value))
        {
            return value;
        }

        var compiledHAndlebarTemplate = new CompiledHandlebarTemplate(template.Subject, template.Body);
        _cachedTemplates.Add(template.Id, compiledHAndlebarTemplate);
        return compiledHAndlebarTemplate;
    }

    private class CompiledHandlebarTemplate(string subjectTemplate, string bodyTemplate)
    {
        public int MyProperty { get; set; }
        public HandlebarsTemplate<object, object> Subject { get; private init; } = Handlebars.Compile(subjectTemplate);
        public HandlebarsTemplate<object, object> Body { get; private init; } = Handlebars.Compile(bodyTemplate);
    }
}