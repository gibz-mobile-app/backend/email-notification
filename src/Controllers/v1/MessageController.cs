using EmailNotification.Models;
using EmailNotification.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmailNotification.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class MessageController(EmailNotificationDbContext dbContext) : ControllerBase
{
    private readonly EmailNotificationDbContext _dbContext = dbContext;

    [HttpPost]
    public async Task<IActionResult> ScheduleEmailNotification(IEnumerable<MessageSchedulingRequest> schedulingRequests)
    {
        foreach (var schedulingRequest in schedulingRequests)
        {
            // Check whether requested template exists
            var template = await _dbContext.Templates.FirstOrDefaultAsync(template => template.NameIdentifier.Equals(schedulingRequest.TemplateIdentifier));
            if (template is null)
            {
                return NotFound($"No template with name identifier '{schedulingRequest.TemplateIdentifier}' found.");
            }

            // Create entity and persist in database
            var message = new Message
            {
                TemplateId = template.Id,
                Recipient = schedulingRequest.Recipient,
                MessageData = schedulingRequest.MessageData,
                ScheduledSendDateTime = schedulingRequest.ScheduledSendDateTime
            };
            await _dbContext.EmailNotifications.AddAsync(message);
            await _dbContext.SaveChangesAsync();
        }

        return NoContent();
    }
}