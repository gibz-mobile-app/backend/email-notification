using EmailNotification.Utilities;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;

namespace EmailNotification.Services;

public class SmtpDispatchService : IMessageDispatchService, IDisposable
{
    private readonly IConfiguration _configuration;
    private readonly SmtpConnectionConfiguration _smtpConnectionConfiguration;
    private readonly SmtpClient _smtpClient;

    public SmtpDispatchService(IConfiguration configuration, IOptions<SmtpConnectionConfiguration> smtpConnectionConfiguration)
    {
        _configuration = configuration;
        _smtpConnectionConfiguration = smtpConnectionConfiguration.Value;

        _smtpClient = new SmtpClient();
        _smtpClient.Connect(_smtpConnectionConfiguration.Server, _smtpConnectionConfiguration.Port, MailKit.Security.SecureSocketOptions.StartTls);
        _smtpClient.Authenticate(_smtpConnectionConfiguration.Username, _smtpConnectionConfiguration.Password);
    }

    public async Task SendEmailAsync(System.Net.Mail.MailAddress recipient, string subject, string body)
    {
        // Compose message object
        var message = new MimeMessage
        {
            Subject = subject,
            Body = new TextPart("html") { Text = body },
        };
        var recipientAddress = GetRecipient(recipient);
        message.To.Add(recipientAddress);
        message.From.Add(new MailboxAddress(_smtpConnectionConfiguration.SenderName, _smtpConnectionConfiguration.SenderAddress));

        // Send message
        await _smtpClient.SendAsync(message); 
    }

    public void Dispose()
    {
        _smtpClient.Disconnect(true);
        GC.SuppressFinalize(this);
    }

    private MailboxAddress GetRecipient(System.Net.Mail.MailAddress recipient)
    {
        var address = _configuration.GetValue<string>("FixDebuggingRecipientAddress");
        if (string.IsNullOrWhiteSpace(address))
        {
            address = recipient.Address;
        }
        return new MailboxAddress(recipient.DisplayName, address);
    }
}