using System.Net.Mail;

namespace EmailNotification.Services;

public interface IMessageDispatchService
{
    public Task SendEmailAsync(MailAddress recipient, string subject, string body);
}