using System.Net.Mail;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace EmailNotification.Utilities;

public class MailAddressConverter : JsonConverter<MailAddress>
{
    public override MailAddress? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        if (reader.TokenType != JsonTokenType.StartObject)
        {
            throw new JsonException();
        }

        Dictionary<string, string> properties = new Dictionary<string, string> {
            {"DisplayName", ""},
            {"User", ""},
            {"Host", ""},
            {"Address", ""},
        };

        string? propertyName;
        string? propertyValue;
        while (reader.Read())
        {
            if (reader.TokenType == JsonTokenType.PropertyName)
            {
                propertyName = reader.GetString();
                if (propertyName != null && properties.ContainsKey(propertyName))
                {
                    reader.Read();
                    if (reader.TokenType == JsonTokenType.String)
                    {
                        propertyValue = reader.GetString();
                        if (!string.IsNullOrEmpty(propertyValue))
                        {
                            properties[propertyName] = propertyValue;
                        }
                    }
                }
            }
            else if (reader.TokenType == JsonTokenType.EndObject)
            {
                return new MailAddress(properties["Address"], properties["DisplayName"]);
            }
        }

        throw new JsonException();
    }

    public override void Write(Utf8JsonWriter writer, MailAddress value, JsonSerializerOptions options)
    {
        throw new NotImplementedException();
    }
}