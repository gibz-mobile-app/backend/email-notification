namespace EmailNotification.Utilities;

public class SmtpConnectionConfiguration
{
    public string Server { get; set; } = null!;
    public int Port { get; set; }
    public string Username { get; set; } = null!;
    public string Password { get; set; } = null!;
    public string SenderName { get; set; } = null!;
    public string SenderAddress { get; set; } = null!;
}